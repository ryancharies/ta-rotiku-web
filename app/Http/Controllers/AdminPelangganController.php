<?php namespace App\Http\Controllers;

use crocodicstudio\crudbooster\controllers\CBController;

class AdminPelangganController extends CBController {


    public function cbInit()
    {
        $this->setTable("pelanggan");
        $this->setPermalink("pelanggan");
        $this->setPageTitle("Pelanggan");

        $this->addDatetime("Terdaftar sejak","created_at")->required(false)->showIndex(false)->showAdd(false)->showEdit(false)->format('d F Y H:i');
		$this->addDatetime("Updated At","updated_at")->required(false)->showIndex(false)->showDetail(false)->showAdd(false)->showEdit(false);
		$this->addText("Nama","nama")->strLimit(150)->maxLength(255);
		$this->addEmail("Email","email");
		$this->addPassword("Password","password");
		$this->addText("Handphone","handphone")->strLimit(15)->maxLength(15);
		$this->addMoney("Saldo","saldo")->prefix('Rp')->thousandSeparator('.')->decimalSeparator(',');
		

    }
}
