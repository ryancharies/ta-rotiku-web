<?php namespace App\Http\Controllers;

use crocodicstudio\crudbooster\controllers\CBController;

class AdminVoucherController extends CBController {


    public function cbInit()
    {
        $this->setTable("voucher");
        $this->setPermalink("voucher");
        $this->setPageTitle("Voucher");

        $this->addDatetime("Tanggal Cetak","created_at")->required(false)->showIndex(false)->showAdd(false)->showEdit(false);
		$this->addDatetime("Updated At","updated_at")->required(false)->showIndex(false)->showDetail(false)->showAdd(false)->showEdit(false);
		$this->addText("Kode","kode")->strLimit(150)->maxLength(10);
		$this->addMoney("Nominal","nominal")->prefix('Rp')->thousandSeparator('.')->decimalSeparator(',');
		$this->addDatetime("Tanggal Pemakaian","terpakai_at")->required(false)->format('d F Y H:i');
		
        


    }
}
