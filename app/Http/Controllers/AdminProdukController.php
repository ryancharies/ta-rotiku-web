<?php namespace App\Http\Controllers;

use crocodicstudio\crudbooster\controllers\CBController;

class AdminProdukController extends CBController {


    public function cbInit()
    {
        $this->setTable("produk");
        $this->setPermalink("produk");
        $this->setPageTitle("Produk");

        $this->addDatetime("Created At","created_at")->required(false)->showIndex(false)->showDetail(false)->showAdd(false)->showEdit(false);
		$this->addDatetime("Updated At","updated_at")->required(false)->showIndex(false)->showDetail(false)->showAdd(false)->showEdit(false);
		$this->addText("Kode","kode")->strLimit(150)->maxLength(255);
		$this->addText("Keterangan","keterangan")->strLimit(150)->maxLength(255);
		$this->addImage("Gambar","gambar")->required(false)->encrypt(true);
		$this->addSelectOption("Status","status")->options(['tersedia'=>'Tersedia','habis'=>'Habis']);
		

    }
}
