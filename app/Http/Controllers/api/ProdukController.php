<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use crocodicstudio\crudbooster\controllers\CBController;

class ProdukController extends Controller
{
    public function data()
    {
        # code...
        $data = cb()->read('produk');

        foreach ($data as $value) {
            $value->gambar = url('/').'/'.$value->gambar;
        }

        return cb()->resp('List all produk', $data);
    }
}
