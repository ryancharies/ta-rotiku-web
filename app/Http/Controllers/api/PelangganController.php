<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Hash;
use crocodicstudio\crudbooster\controllers\CBController;

class PelangganController extends Controller
{
    private $table = 'pelanggan';

    public function login(Request $request)
    {
        # code...
        $json = $request->json()->all();
        $pelanggan = cb()->find('pelanggan',['email' => $json['email']]);

        if(!empty($pelanggan)){
            return cb()->resp('Login berhasil', $pelanggan);
        }else{
            return cb()->resp('Login gagal, user tidak ditemukan', null, true, 404);
        }
    }

    public function registrasi(Request $request)
    {
        # code...
        $json = $request->json()->all();                
        $insert = cb()->insGetId($this->table, $json);

        if(!empty($insert)){
            $pelanggan = cb()->find('pelanggan', $insert);            
            return cb()->resp('Registrasi berhasil', $pelanggan);
        }else{
            return cb()->resp('Registrasi gagal', null, true, 500);
        }
    }

    public function ganti_password(Request $request)
    {
        # code...
        $json = $request->json()->all();
        $pelanggan = cb()->find('pelanggan', ['email' => $json['email']]);
        
        try {            
            $update = cb()->update('pelanggan', $pelanggan->id, ['password' => Hash::make($json['password'])]);            
        } catch (\Exception $e) {            
            return cb()->resp('Ganti password gagal', $e, true, 500);
        }

        return cb()->resp('Ganti password berhasil', null);        
    }
}
