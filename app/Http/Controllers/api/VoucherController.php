<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use crocodicstudio\crudbooster\controllers\CBController;

class VoucherController extends Controller
{
    public function topup(Request $request)
    {
        $json = $request->json()->all();
        # code...
        try {
            $vc = cb()->find('voucher', ['kode' => $json['voucher']]);
            if(is_null($vc->terpakai_at)){
                $pelanggan = cb()->find('pelanggan', ['email' => $json['email']]);
                $saldo = (int)$pelanggan->saldo + (int)$vc->nominal;

                // update saldo pelanggan
                cb()->update('pelanggan', $pelanggan->id, ['saldo' => $saldo]);
                // update voucher terpakai
                cb()->update('voucher', $vc->id, ['terpakai_at' =>  cb()->now()]);

                $pelanggan = cb()->find('pelanggan', ['email' => $json['email']]);
            }else{
                return cb()->resp('Voucher tidak valid / sudah terpakai', null, true, 200);    
            }
        } catch (\Exception $e) {
            return cb()->resp('Voucher gagal di topup', $e, true, 500);
        }

        return cb()->resp('Voucher berhasil di topup', $pelanggan);
    
    }
}
