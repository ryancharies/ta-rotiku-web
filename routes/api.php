<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('pelanggan')->group(function(){
    Route::get('login', 'api\PelangganController@login');
    Route::post('registrasi', 'api\PelangganController@registrasi');
    Route::post('ganti_password', 'api\PelangganController@ganti_password');
});

Route::prefix('voucher')->group(function(){
    Route::post('topup', 'api\VoucherController@topup');
});

Route::prefix('produk')->group(function(){
    Route::get('/', 'api\ProdukController@data');
});